<?php

define('BASEPATH', '');
define('ENVIRONMENT', '');

require_once("../application/config/database.php");
$dbconn = $db[$active_group];

$conn = mysqli_connect( $dbconn['hostname'], $dbconn['username'], $dbconn['password'], $dbconn['database']);

function slugify($text)
{ 
  // replace non letter or digits by -
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

  // trim
  $text = trim($text, '-');

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // lowercase
  $text = strtolower($text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  if (empty($text))
  {
    return 'n-a';
  }

  return $text;
}

function getLatest() {
		global $conn;
		$isql = "SELECT `id` FROM `places` ORDER BY `id` DESC LIMIT 1;";
		$query = mysqli_query( $conn, $isql );
		$fetch = ($query) ? mysqli_fetch_object( $query ) : false;
		if( $fetch ) {
			return intval( $fetch->id ) + 1;
		} else {
			return 1;
		}
}

function insertPageId( $id, $page_id ) {
		global $conn;
		$isql = "INSERT INTO `places` (`id`,`page_id`) VALUES ('%s','%s');";
		$sql = sprintf($isql, $id, $page_id);
		mysqli_query( $conn, $sql );
}

function dotconfig($write=false,$value='') {
	if( $write ) {
		$f = fopen('.heyplaces', 'w');
		fwrite($f, $value);
		fclose($f);
	} else {
		$f = fopen('.heyplaces', 'r');
		$v = fread($f, 1028);	
		fclose($f);
		return $v;
	}
}

function fetch( $last_id ) {
	
	echo $url = 'http://heyplaces.ph/0'.$last_id.'/abcdefghijklmnopqrstuvwxyz';
	echo "\n";
	libxml_use_internal_errors(true);
	$dom = new DomDocument;
	if( @$dom->loadHTMLFile($url) ) {
		$xpath = new DomXPath($dom);
		$imgNodes = $xpath->query("//meta[@property='og:image']");
		foreach( $imgNodes as $imgNode ) {
			$img = $imgNode->getAttribute('content');
		}
		echo $img . "\n";
		
		if( strpos( $img, 'http://heyplaces.ph/resize.jpg?id=' ) === false ) {
			echo "page not found!";
			insertPageId( $last_id, $last_id );
		} else {
			echo $page_id = str_replace('http://heyplaces.ph/resize.jpg?id=', '', $img);
			insertPageId( $last_id, $page_id );
		}
		
	} 
	echo "\n";
	//dotconfig(true, (intval($last_id) + 1));
}

function fetch2( $last_id ) {
	
	echo $url = 'http://heyplaces.ph/0'.$last_id.'/page_slug_here';
	echo "\n";

	$html =  file_get_contents($url, null, null, 0, 1500);
	
	$html_arr1 = explode('http://heyplaces.ph/resize.jpg?id=', $html);
	if( isset( $html_arr1[1] ) ) {
		$html_arr2 = explode('"', $html_arr1[1]);
		if( isset( $html_arr2[0] ) && (trim($html_arr2[0]) != '') ) {
			insertPageId( $last_id, $html_arr2[0] );
			echo $html_arr2[0];
		} else {
			insertPageId( $last_id, $last_id );
			echo "page not found!";
		}
	}
	echo "\n";
	
}

//$last_id = dotconfig();
if( isset($_GET['id']) && ($_GET['id'] != '' ) ) {
	$last_id = $_GET['id'];
} else {
	$last_id = getLatest();
}

fetch2( intval($last_id) );
fetch2( intval($last_id) + 1 );
