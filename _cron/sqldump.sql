-- Table structure for table `accounts` 

CREATE TABLE `accounts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`email`)
);

-- Table structure for table `categories` 

CREATE TABLE `categories` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `cat_id` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `count` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cat_id` (`cat_id`),
  UNIQUE KEY `id` (`id`)
);

-- Table structure for table `ci_sessions` 

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
);

-- Table structure for table `city` 

CREATE TABLE `city` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `country` 

CREATE TABLE `country` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `places` 

CREATE TABLE `places` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `page_id` varchar(100) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `modby` int(20) DEFAULT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page_id` (`page_id`)
);

-- Table structure for table `places_category` 

CREATE TABLE `places_category` (
  `id` int(20) NOT NULL,
  `cat_id` int(20) NOT NULL,
  KEY `id` (`id`,`cat_id`)
);

-- Table structure for table `places_description` 

CREATE TABLE `places_description` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`)
);

-- Table structure for table `places_location` 

CREATE TABLE `places_location` (
  `id` int(20) NOT NULL,
  `city_id` int(20) DEFAULT NULL,
  `country_id` int(20) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`)
);

-- Table structure for table `places_picture` 

CREATE TABLE `places_picture` (
  `id` int(20) NOT NULL,
  `url` text NOT NULL,
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`)
);

