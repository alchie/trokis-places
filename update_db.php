<?php

define('BASEPATH', '');
define('ENVIRONMENT', '');

require_once("application/config/database.php");
$dbconn = $db[$active_group];

$conn = mysqli_connect( $dbconn['hostname'], $dbconn['username'], $dbconn['password'], $dbconn['database']);

$sqls = array(
	"CREATE TABLE IF NOT EXISTS `places_meta`(`id` int(20) NOT NULL AUTO_INCREMENT,  `place_id` int(20) NOT NULL,  `meta_key` varchar(200) NOT NULL,  `meta_value` text,  `active` int(1) DEFAULT '1',PRIMARY KEY (`id`));",
);

if( count($sqls) > 0) {
	foreach($sqls as $sql) {
		mysqli_query( $conn, $sql );
		echo $sql . "<br>";
	}
}
echo "done!";