<?php $this->load->view('header'); ?>
        <div class="col-md-8 col-sm-8 blog-main">

    <div class="row">
        <div class="panel panel-default widget">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-map-marker"></span>
                <h3 class="panel-title">
                    Latests Places</h3>
                <span class="label label-info"><?php echo $total_places; ?></span>
            </div>
            <div class="panel-body">
                <ul class="list-group">
				<?php foreach( $latest_places as $place ) { ?>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-4 col-md-2">
                                <img src="<?php echo ($place->url!='') ? $place->url : 'http://placehold.it/50'; ?>" class="img-circle img-responsive" alt="" /></div>
                            <div class="col-xs-8 col-md-10">
                                <div>
                                    <a href="<?php echo site_url($place->id . "_" . url_title($place->name)); ?>">
                                        <?php echo $place->name; ?></a>
                                    <div class="mic-info">
                                        By: <?php echo $place->user_name; ?> on <?php echo date("d M, Y", strtotime($place->lastmod)); ?>
                                    </div>
                                </div>
                                <div class="comment-text">
                                    <?php echo substr( $place->description, 0, 160); ?>
                                </div>
                            </div>
                        </div>
                    </li>
				<?php } ?>
				</ul>
            </div>
        </div>
    </div>


        </div><!-- /.blog-main -->

        <?php $this->load->view('sidebar'); ?>
		<?php $this->load->view('footer'); ?>