<?php $this->load->view('header'); ?>
        <div class="col-md-8 col-sm-8 blog-main">


    <div class="blog-post">
			
            <h3 class="blog-post-title"><img src="<?php echo $place['picture']['data']['url']; ?>"> <?php echo $place['name']; ?></h3>
            <p class="blog-post-meta"><?php echo @$place['location']['city']; ?>, <?php echo @$place['location']['country']; ?></p>
			<p> <?php echo @$place['description']; ?></p>
	</div>


        </div><!-- /.blog-main -->

        <?php $this->load->view('sidebar'); ?>
		<?php $this->load->view('footer'); ?>