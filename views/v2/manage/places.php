<?php $this->load->view('header'); ?>
                    <div class="full col-sm-9">
                        <!-- content -->                      
                      	<div class="row">
                          
                         <!-- main col left --> 
                         <div class="col-sm-12">
                              <div class="panel panel-default">
                                <div class="panel-heading">
					
					<form class="pull-right" method="get" >
                        <div class="input-group input-group-sm" style="max-width:360px;">
                          <input type="text" class="form-control" placeholder="Search" name="q" value="<?php echo $this->input->get('q'); ?>">
                          <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                          </div>
                        </div>
                    </form>
								<h6>Places</h6></div>
                                  <div class="panel-body">
								  
<div class="table-responsive">				  
<table class="table table-striped">
	<thead>
        <tr>
          <th>#</th>
          <th>FB Page ID</th>
          <th>Name</th>
          <th>Type</th>
          <th width="170px">Action</th>
        </tr>
      </thead>
	  <tbody>
	  <?php foreach( $all_places as $place ) { ?>
        <tr>
          <th scope="row"><?php echo $place->id; ?></th>
          <td><?php echo $place->page_id; ?></td>
          <td><?php echo $place->name; ?></td>
		      <td><a href="<?php echo site_url("manage/places") . "?type=" . $place->type; ?>"><?php echo $place->type; ?></a></td>
          <td><a target="update_place" href="<?php echo site_url("browse/fetch/" . $place->id . "/" . $place->page_id); ?>" class="btn btn-warning btn-xs">Fetch</a> <a href="<?php echo site_url("manage/places/edit/" . $place->id); ?>" class="btn btn-success btn-xs">Edit</a> <a href="<?php echo site_url("manage/places/delete/" . $place->id); ?>" class="btn btn-danger btn-xs">Delete</a></td>
        </tr>
	  <?php } ?>
      </tbody>
</table>
<?php echo $pagination; ?>
</div>

                                  </div>
                              </div>
                          </div>

                         </div><!--/row-->
                        
                    </div><!-- /col-9 -->
<?php $this->load->view('footer'); ?>