<?php $this->load->view('header'); ?>
                    <div class="full col-sm-9">
                        <!-- content -->                      
                      	<div class="row">
                          
                         <!-- main col left --> 
                         <div class="col-sm-6 col-sm-offset-3">
				<form method="post" action="">
                              <div class="panel panel-default">
                                <div class="panel-heading">
								<h6><?php echo $current_city->name; ?></h6>
								</div>
                                  <div class="panel-body">
					<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() .'</div>' : ''; ?>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo $current_city->name; ?>"  required autofocus>
					  </div>
					  

                                  </div>
								<div class="panel-footer">
										<button type="submit" class="btn btn-success">Update</button> <a class="btn btn-danger" href="<?php echo site_url('manage/cities'); ?>">Cancel</a>
								</div>
                              </div>
				</form>
                          </div>

                         </div><!--/row-->
                        
                    </div><!-- /col-9 -->
<?php $this->load->view('footer'); ?>