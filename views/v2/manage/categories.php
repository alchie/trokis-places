<?php $this->load->view('header'); ?>
                    <div class="full col-sm-9">
                        <!-- content -->                      
                      	<div class="row">
                          
                         <!-- main col left --> 
                         <div class="col-sm-12">
                              <div class="panel panel-default">
                                <div class="panel-heading">
													<form class="pull-right" method="get" >
                        <div class="input-group input-group-sm" style="max-width:360px;">
                          <input type="text" class="form-control" placeholder="Search" name="q" value="<?php echo $this->input->get('q'); ?>">
                          <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                          </div>
                        </div>
                    </form>
								
								<h6>Categories</h6>
								
								</div>
                                  <div class="panel-body">
								  
<div class="table-responsive">				  
<table class="table table-striped">
	<thead>
        <tr>
          <th>#</th>
          <th>FB Cat ID</th>
          <th>Name</th>
          <th width="110px">Action</th>
        </tr>
      </thead>
	  <tbody>
	  <?php foreach( $all_categories as $category ) { ?>
        <tr>
          <th scope="row"><?php echo $category->id; ?></th>
          <td><?php echo $category->cat_id; ?></td>
          <td><?php echo $category->name; ?></td>
          <td><a href="<?php echo site_url("manage/categories/edit/" . $category->id); ?>" class="btn btn-success btn-xs">Edit</a> <a href="<?php echo site_url("manage/categories/delete/" . $category->id); ?>" class="btn btn-danger btn-xs">Delete</a></td>
        </tr>
	  <?php } ?>
      </tbody>
</table>
<?php echo $pagination; ?>
</div>

                                  </div>
                              </div>
                          </div>

                         </div><!--/row-->
                        
                    </div><!-- /col-9 -->
<?php $this->load->view('footer'); ?>