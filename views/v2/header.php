<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title><?php echo $page_title; ?></title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="google-site-verification" content="Jdnn-Kcwnm6pdMfyLC0NyEIFJR-rxSKZAzkNiB8iFq4" />
		<link href="<?php echo base_url("assets/v2/css/bootstrap.min.css"); ?>" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="<?php echo base_url("assets/v2/css/styles.css"); ?>" rel="stylesheet">
	</head>
	<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
			<?php if( isset( $cities ) ) : ?>
            <!-- sidebar -->
            <div class="column col-sm-2 col-xs-1 sidebar-offcanvas" id="sidebar">
              
              	<ul class="nav">
          			<li><a href="#" data-toggle="offcanvas" class="visible-xs text-center"><i class="glyphicon glyphicon-chevron-right"></i></a></li>
            	</ul>
               
                <ul class="nav hidden-xs" id="lg-menu">
					<?php foreach( $cities as $city ) { ?>
						<li><a href="<?php echo site_url( "city/" . $city->id . "_" . url_title($city->name) ); ?>"><i class="glyphicon glyphicon-list"></i> <?php echo $city->name; ?> City</a></li>
					<?php } ?>
                </ul>
              
              	<!-- tiny only nav-->
              <ul class="nav visible-xs" id="xs-menu">
                  	<li><a href="#featured" class="text-center"><i class="glyphicon glyphicon-list-alt"></i></a></li>
                    <li><a href="#stories" class="text-center"><i class="glyphicon glyphicon-list"></i></a></li>
                  	<li><a href="#" class="text-center"><i class="glyphicon glyphicon-paperclip"></i></a></li>
                    <li><a href="#" class="text-center"><i class="glyphicon glyphicon-refresh"></i></a></li>
                </ul>
              
            </div>
            <!-- /sidebar -->
			<!-- main right col -->
			<div class="column col-sm-10 col-xs-11" id="main">
			<?php else: ?>
			
			<!-- main right col -->
			<div class="column col-sm-12 col-xs-12" id="main">
			
			<?php endif; ?>

                <!-- top nav -->
              	<div class="navbar navbar-blue navbar-static-top">  
                    <div class="navbar-header">
                      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle</span>
                        <span class="icon-bar"></span>
          				<span class="icon-bar"></span>
          				<span class="icon-bar"></span>
                      </button>
                      <a href="/" class="navbar-brand logo"><img src="<?php echo base_url("assets/v2/icons/favicon-28x28.png"); ?>"></a>
                  	</div>
                  	<nav class="collapse navbar-collapse" role="navigation">
                    <form class="navbar-form navbar-left" method="get">
                        <div class="input-group input-group-sm" style="max-width:360px;">
                          <input type="text" class="form-control" placeholder="Search" name="q">
                          <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                          </div>
                        </div>
                    </form>
                    <ul class="nav navbar-nav">
                      <li><a href="<?php echo site_url(); ?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                      <li><a href="<?php echo site_url("share_and_win"); ?>"><i class="glyphicon glyphicon-heart-empty"></i> Share and Win</a></li>
                      <li><a href="<?php echo site_url("random-place"); ?>"><i class="glyphicon glyphicon-random"></i> Random Place</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
					<?php if( $logged_in ) { ?>
					<?php if( $this->session->userdata( 'id' ) == '10208772272039472') { ?>
						 <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage</a>
							<ul class="dropdown-menu">
							  <li><a href="<?php echo site_url("manage/places"); ?>">Places</a></li>
							  <li><a href="<?php echo site_url("manage/cities"); ?>">Cities</a></li>
							  <li><a href="<?php echo site_url("manage/categories"); ?>">Categories</a></li>
							  <li><a href="<?php echo site_url("manage/users"); ?>">Users</a></li>
							</ul>
						 </li>
					<?php } ?>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> <?php echo $this->session->userdata( 'name' ); ?></a>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo site_url("logout"); ?>">Logout</a></li>
                        </ul>
                      </li>
					  <?php } else { ?>
					  <li><a href="<?php echo site_url("login"); ?>"><i class="glyphicon glyphicon-log-in"></i> Facebook</a></li>
					  <?php }  ?>
                    </ul>
                  	</nav>
                </div>
                <!-- /top nav -->
              
                <div class="padding">