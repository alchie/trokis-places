<?php $this->load->view('header'); 
$empty_description = $place->name . ' is a ';
$cat = array();
foreach( $categories as $category ) {
	$cat[] = $category->name;
}
$empty_description .= implode(' and a ', $cat) . ' in the Philippines.';
?>
<div class="full col-sm-9">
	<div class="row">
		<div class="col-sm-9">
			<div class="panel panel-default">
				<div class="panel-heading">
				<!--<a class="btn btn-danger btn-xs pull-right" href="<?php echo site_url( $place->id . "_" . url_title($place->name) . "_map"); ?>">Show Map</a>-->
				<div class="pull-right">
				<!--<div class="fb-share-button" data-href="<?php echo site_url( $place->id . "_" . url_title($place->name)); ?>" data-layout="button" data-size="small" data-mobile-iframe="false"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( site_url( $place->id . "_" . url_title($place->name)) ); ?>&amp;src=sdkpreparse">Share</a></div>-->
				<div class="fb-like" data-href="<?php echo site_url( $place->id . "_" . url_title($place->name)); ?>" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
				</div>
				<?php if( $this->session->userdata( 'id' ) == '10208772272039472') { ?>
					<a style="margin-right:5px;" class="btn btn-success btn-xs pull-right" href="<?php echo site_url( "manage/places/edit/" . $place->id ); ?>">Edit this place</a>
				<?php } ?>
				<h3 class="panel-title">
				<?php echo $place->name; ?></h3>
				</div>
				<div class="panel-body">
				<?php echo ($place->url!='') ? '<img class="pull-right thumbnail" src="'.$place->url.'">' : ''; ?>
				<p><?php echo ($place->description!='') ? $place->description : $empty_description; ?></p>
				</div>
			</div>
			
<div class="panel panel-default"><div class="panel-body">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Trokis Responsive -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="8482696123"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div></div>

<div class="row">
	<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
				<h3 class="panel-title">Categories</h3>
				</div>
				<div class="panel-body">
					<div class="list-group">
						<?php foreach( $categories as $category ) { ?>
							<a href="<?php echo site_url("category/" . $category->id . "_" . url_title($category->name)); ?>" class="list-group-item"><?php echo $category->name; ?></a>
						<?php } ?>
					</div>
				</div>
			</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="fb-page" data-href="https://www.facebook.com/<?php echo $place->page_id; ?>/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
			</div>
		</div>
	</div>
</div>
			
<div class="panel panel-default"><div class="panel-body">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Trokis Responsive -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="8482696123"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div></div>

		</div>
		<div class="col-sm-3">
			<div class="panel panel-default">
			<div class="panel-body">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- Trokis Responsive -->
			<ins class="adsbygoogle"
				 style="display:block"
				 data-ad-client="ca-pub-7233800271694028"
				 data-ad-slot="8482696123"
				 data-ad-format="auto"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
			</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('footer'); ?>