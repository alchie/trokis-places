<?php $this->load->view('header'); ?>
                    <div class="full col-sm-9">
                      
                        <!-- content -->                      
                      	<div class="row">
                          
                         <!-- main col left --> 
						 <?php foreach( $categories as $category ) { ?>
                         <div class="col-sm-4">
                              <div class="panel panel-default">
                                <div class="panel-heading"><a href="<?php echo site_url("category/" . $category->id . "_" . url_title($category->name)); ?>" class="pull-right">View all</a> <h4><?php echo $category->name; ?></h4></div>
                                  <div class="panel-body">
                                    <div class="list-group">
										<?php foreach( $category->places as $place ) { ?>
											<a href="<?php echo site_url($place->id . "_" . urlencode( url_title($place->name) ) ); ?>" class="list-group-item"><?php echo $place->name; ?></a>
										<?php } ?>
                                    </div>
                                  </div>
                              </div>
                          </div>
						 <?php } ?>
                          <!-- main col right -->
                         </div><!--/row-->
                        
                      
                    </div><!-- /col-9 -->
<?php $this->load->view('footer'); ?>