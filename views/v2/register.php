<?php $this->load->view('header'); ?>

<div class="full col-sm-9">
<!-- content -->                      
	<div class="row">
		<div class="col-md-12">
		 <form method="post" action="<?php echo site_url('account/register'); ?>">
        <div class="panel panel-default widget">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-envelope"></span> New User</h3>
            </div>
            <div class="panel-body">
               
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo $name; ?>" required autofocus>
					  </div>
					  <div class="form-group">
						<input type="email" class="form-control" placeholder="Email Address" name="email" value="<?php echo $email; ?>" required>
					  </div>
            </div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-success">Register</button>
			</div>
        </div>
		 </form>
        </div>
    </div>

</div><!-- /.blog-main -->

<?php $this->load->view('footer'); ?>