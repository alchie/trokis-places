<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Places_model Class
 *
 * Manipulates `places` table on database

CREATE TABLE `places` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `page_id` varchar(100) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `modby` int(20) DEFAULT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page_id` (`page_id`)
);

 * @package			Model
 * @project			Trokis Places
 * @project_link	http://places.trokis.com/
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com/
 */
 
class Places_model extends CI_Model {

	protected $id;
	protected $page_id;
	protected $name;
	protected $type;
	protected $modby;
	protected $lastmod;
	protected $_short_name = 'places';
	protected $_dataFields = array();
	protected $_select = array();
	protected $_join = array();
	protected $_where = array();
	protected $_where_or = array();
	protected $_where_in = array();
	protected $_where_in_or = array();
	protected $_where_not_in = array();
	protected $_where_not_in_or = array();
	protected $_like = array();
	protected $_like_or = array();
	protected $_like_not = array();
	protected $_like_not_or = array();
	protected $_having = array();
	protected $_having_or = array();
	protected $_group_by = array();
	protected $_filter = array();
	protected $_order = array();
	protected $_exclude = array();
	protected $_required = array();
	protected $_countField = '';
	protected $_start = 0;
	protected $_limit = 10;
	protected $_results = FALSE;
	protected $_distinct = FALSE;
	protected $_cache_on = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name='places') {
		parent::__construct();
		$this->_short_name = $short_name;
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->id = ( ($value == '') && ($this->id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'id';
		}
		return $this;
	}

	/** 
	* Get the value of `id` variable
	* @access public
	* @return String;
	*/

	public function getId() {
		return $this->id;
	}

	/**
	* Get row by `id`
	* @param id
	* @return QueryResult
	**/

	public function getById() {
		if($this->id != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('places ' . $this->_short_name, array( $this->_short_name . '.id' => $this->id), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `id`
	**/

	public function updateById() {
		if($this->id != '') {
			$this->setExclude('id');
			if( $this->getData() ) {
				return $this->db->update('places '. $this->_short_name, $this->getData(), array('places.id' => $this->id ) );
			}
		}
	}


	/**
	* Delete row by `id`
	**/

	public function deleteById() {
		if($this->id != '') {
			return $this->db->delete('places', array('places.id' => $this->id ) );
		}
	}

	/**
	* Increment row by `id`
	**/

	public function incrementById() {
		if($this->id != '' && $this->_countField != '') {
			$this->db->where('id', $this->id);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `id`
	**/

	public function decrementById() {
		if($this->id != '' && $this->_countField != '') {
			$this->db->where('id', $this->id);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: page_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `page_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setPageId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->page_id = ( ($value == '') && ($this->page_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.page_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'page_id';
		}
		return $this;
	}

	/** 
	* Get the value of `page_id` variable
	* @access public
	* @return String;
	*/

	public function getPageId() {
		return $this->page_id;
	}

	/**
	* Get row by `page_id`
	* @param page_id
	* @return QueryResult
	**/

	public function getByPageId() {
		if($this->page_id != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('places ' . $this->_short_name, array( $this->_short_name . '.page_id' => $this->page_id), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `page_id`
	**/

	public function updateByPageId() {
		if($this->page_id != '') {
			$this->setExclude('page_id');
			if( $this->getData() ) {
				return $this->db->update('places '. $this->_short_name, $this->getData(), array('places.page_id' => $this->page_id ) );
			}
		}
	}


	/**
	* Delete row by `page_id`
	**/

	public function deleteByPageId() {
		if($this->page_id != '') {
			return $this->db->delete('places', array('places.page_id' => $this->page_id ) );
		}
	}

	/**
	* Increment row by `page_id`
	**/

	public function incrementByPageId() {
		if($this->page_id != '' && $this->_countField != '') {
			$this->db->where('page_id', $this->page_id);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `page_id`
	**/

	public function decrementByPageId() {
		if($this->page_id != '' && $this->_countField != '') {
			$this->db->where('page_id', $this->page_id);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: page_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: name
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->name = ( ($value == '') && ($this->name != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.name';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'name';
		}
		return $this;
	}

	/** 
	* Get the value of `name` variable
	* @access public
	* @return String;
	*/

	public function getName() {
		return $this->name;
	}

	/**
	* Get row by `name`
	* @param name
	* @return QueryResult
	**/

	public function getByName() {
		if($this->name != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('places ' . $this->_short_name, array( $this->_short_name . '.name' => $this->name), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `name`
	**/

	public function updateByName() {
		if($this->name != '') {
			$this->setExclude('name');
			if( $this->getData() ) {
				return $this->db->update('places '. $this->_short_name, $this->getData(), array('places.name' => $this->name ) );
			}
		}
	}


	/**
	* Delete row by `name`
	**/

	public function deleteByName() {
		if($this->name != '') {
			return $this->db->delete('places', array('places.name' => $this->name ) );
		}
	}

	/**
	* Increment row by `name`
	**/

	public function incrementByName() {
		if($this->name != '' && $this->_countField != '') {
			$this->db->where('name', $this->name);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `name`
	**/

	public function decrementByName() {
		if($this->name != '' && $this->_countField != '') {
			$this->db->where('name', $this->name);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: name
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: type
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->type = ( ($value == '') && ($this->type != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.type';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'type';
		}
		return $this;
	}

	/** 
	* Get the value of `type` variable
	* @access public
	* @return String;
	*/

	public function getType() {
		return $this->type;
	}

	/**
	* Get row by `type`
	* @param type
	* @return QueryResult
	**/

	public function getByType() {
		if($this->type != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('places ' . $this->_short_name, array( $this->_short_name . '.type' => $this->type), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `type`
	**/

	public function updateByType() {
		if($this->type != '') {
			$this->setExclude('type');
			if( $this->getData() ) {
				return $this->db->update('places '. $this->_short_name, $this->getData(), array('places.type' => $this->type ) );
			}
		}
	}


	/**
	* Delete row by `type`
	**/

	public function deleteByType() {
		if($this->type != '') {
			return $this->db->delete('places', array('places.type' => $this->type ) );
		}
	}

	/**
	* Increment row by `type`
	**/

	public function incrementByType() {
		if($this->type != '' && $this->_countField != '') {
			$this->db->where('type', $this->type);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `type`
	**/

	public function decrementByType() {
		if($this->type != '' && $this->_countField != '') {
			$this->db->where('type', $this->type);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: type
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: modby
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `modby` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setModby($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->modby = ( ($value == '') && ($this->modby != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.modby';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'modby';
		}
		return $this;
	}

	/** 
	* Get the value of `modby` variable
	* @access public
	* @return String;
	*/

	public function getModby() {
		return $this->modby;
	}

	/**
	* Get row by `modby`
	* @param modby
	* @return QueryResult
	**/

	public function getByModby() {
		if($this->modby != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('places ' . $this->_short_name, array( $this->_short_name . '.modby' => $this->modby), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `modby`
	**/

	public function updateByModby() {
		if($this->modby != '') {
			$this->setExclude('modby');
			if( $this->getData() ) {
				return $this->db->update('places '. $this->_short_name, $this->getData(), array('places.modby' => $this->modby ) );
			}
		}
	}


	/**
	* Delete row by `modby`
	**/

	public function deleteByModby() {
		if($this->modby != '') {
			return $this->db->delete('places', array('places.modby' => $this->modby ) );
		}
	}

	/**
	* Increment row by `modby`
	**/

	public function incrementByModby() {
		if($this->modby != '' && $this->_countField != '') {
			$this->db->where('modby', $this->modby);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `modby`
	**/

	public function decrementByModby() {
		if($this->modby != '' && $this->_countField != '') {
			$this->db->where('modby', $this->modby);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: modby
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lastmod
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lastmod` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLastmod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lastmod = ( ($value == '') && ($this->lastmod != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = $this->_short_name . '.lastmod';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->_dataFields[] = 'lastmod';
		}
		return $this;
	}

	/** 
	* Get the value of `lastmod` variable
	* @access public
	* @return String;
	*/

	public function getLastmod() {
		return $this->lastmod;
	}

	/**
	* Get row by `lastmod`
	* @param lastmod
	* @return QueryResult
	**/

	public function getByLastmod() {
		if($this->lastmod != '') {
			if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
			}

			$this->setupJoin();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('places ' . $this->_short_name, array( $this->_short_name . '.lastmod' => $this->lastmod), 1, 0);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->_results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lastmod`
	**/

	public function updateByLastmod() {
		if($this->lastmod != '') {
			$this->setExclude('lastmod');
			if( $this->getData() ) {
				return $this->db->update('places '. $this->_short_name, $this->getData(), array('places.lastmod' => $this->lastmod ) );
			}
		}
	}


	/**
	* Delete row by `lastmod`
	**/

	public function deleteByLastmod() {
		if($this->lastmod != '') {
			return $this->db->delete('places', array('places.lastmod' => $this->lastmod ) );
		}
	}

	/**
	* Increment row by `lastmod`
	**/

	public function incrementByLastmod() {
		if($this->lastmod != '' && $this->_countField != '') {
			$this->db->where('lastmod', $this->lastmod);
			$this->db->set($this->_countField, $this->_countField.'+1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}

	/**
	* Decrement row by `lastmod`
	**/

	public function decrementByLastmod() {
		if($this->lastmod != '' && $this->_countField != '') {
			$this->db->where('lastmod', $this->lastmod);
			$this->db->set($this->_countField, $this->_countField.'-1', FALSE);
			$this->db->update('places '. $this->_short_name);
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lastmod
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupGroupBy();
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('places ' . $this->_short_name, 1);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->_results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->_results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('places');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->_dataFields = array($fields);
			} else {
				$this->_dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('places '. $this->_short_name, $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('places', $this->getData() ) === TRUE ) {
				
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('places', $this->getData() ) === TRUE ) {
				
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupGroupBy();
		
		if( $this->_order ) {
			foreach( $this->_order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->_start);
		
		if( $this->_cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('places ' . $this->_short_name);
		
		if( $this->_cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->_results = $result[0];
			
			return $this->_results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->_distinct ) {
			$this->db->distinct();
		}
		
		if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupGroupBy();
		
		if( $this->_order ) {
			foreach( $this->_order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->_limit > 0 ) {
			$this->db->limit( $this->_limit,$this->_start);
		}
		
		if( $this->_cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('places ' . $this->_short_name);
		
		if( $this->_cache_on ) {
			$this->db->cache_off();
		}
			
		$this->_results = $query->result();
		
		return $this->_results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->_limit > 0 ) {
				$this->db->limit( $this->_limit,$this->_start);
			}
			if( $this->_select ) {
					$this->db->select( implode(',' , $this->_select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->_order ) {
				foreach( $this->_order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->_cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('places ' . $this->_short_name);
			
			if( $this->_cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->_where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->_where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = $this->_short_name . '.' . $field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->_where[$key] = $this->$field;
				$this->_where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->_dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("id","page_id","name","type","modby","lastmod");
		if( count( $this->_dataFields ) > 0 ) {
    		            $fields = $this->_dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->_required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->_exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->_required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->_exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->_required ) ) 
		    && ( ! in_array( $field, $this->_exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge( $this->_where, $this->_filter, $this->_where_or, $this->_where_in, $this->_where_in_or, $this->_where_not_in, $this->_where_not_in_or, $this->_like, $this->_like_or, $this->_like_not, $this->_like_not_or, $this->_having, $this->_having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge( $this->_where, $this->_filter, $this->_where_or, $this->_where_in, $this->_where_in_or, $this->_where_not_in, $this->_where_not_in_or, $this->_like, $this->_like_or, $this->_like_not, $this->_like_not_or, $this->_having, $this->_having_or );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->_join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->_join ) {
			foreach( $this->_join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'places'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->_filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_filter);
			$this->_filter = array();
		} else {
			$newfilter = array();
			foreach($this->_filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->_filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where);
			$this->_where = array();
		} else {
			$newwhere = array();
			foreach($this->_where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->_where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_or);
			$this->_where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->_where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->_where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_in);
			$this->_where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->_where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->_where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_in_or);
			$this->_where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->_where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->_where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_not_in);
			$this->_where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->_where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->_where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_where_not_in_or);
			$this->_where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->_where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->_where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like);
			$this->_like = array();
		} else {
			$newlike = array();
			foreach($this->_like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->_like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_or);
			$this->_like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->_like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->_like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_not);
			$this->_like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->_like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->_like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->_like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_like_not_or);
			$this->_like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->_like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->_like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->_having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_having);
			$this->_having = array();
		} else {
			$newhaving = array();
			foreach($this->_having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->_having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->_having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->_having_or);
			$this->_having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->_having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->_having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->_group_by = array_merge( $this->_group_by, $fields );
		} else {
			$this->_group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->_group_by ) {
			$this->db->group_by( $this->_group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->_start = $value;
		return $this;
	}

        // --------------------------------------------------------------------

        /**
	* Get Start  
	* @access public
	*/
	public function getStart() {
		return $this->_start;
	}

        // --------------------------------------------------------------------
        
	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->_limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

        /**
	* Get Limit  
	* @access public
	*/
	public function getLimit() {
		return $this->_limit;
	}

	// --------------------------------------------------------------------
	
	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->_order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_array ( $select ) ) {
			$this->_select = array_merge( $this->_select, $select );
		} else {
			if( is_null( $index ) ) {
				$this->_select[] = $select;
			} else {
				$this->_select[$index] = $select;
			}
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		if( is_array ( $exclude ) ) {
			$this->_exclude = array_merge( $this->_exclude, $exclude );
		} else {
			$this->_exclude[] = $exclude;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->_distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('places ' . $this->_short_name);
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->setupJoin();
		$this->db->from('places ' . $this->_short_name);
		return  $this->db->count_all_results();
	}

	// --------------------------------------------------------------------

	/**
	* SQL Functions  
	* @access public
	*/
	public function sql_function($function, $field, $alias=false, $hasConditions=true) {
                if( $this->_select ) {
				$this->db->select( implode(',' , $this->_select) );
		}
		if( $alias ) {
                        $this->db->select($function.'('.$field.') AS ' . $alias);
		} else {
                        $this->db->select($function.'('.$field.')');
		}
		if( $hasConditions ) {
                        $this->setupJoin();
                        $this->setupConditions();
                        $this->setupGroupBy();
                }
		return  $this->db->get('places ' . $this->_short_name, 1);
	}	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->_cache_on = TRUE;
	}
}

/* End of file places_model.php */
/* Location: ./application/models/places_model.php */
