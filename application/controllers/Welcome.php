<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
	public function index($slug='')
	{
		
		$this->load->model("City_model");
		$cities = new $this->City_model;
		$cities->setLimit(20);
		$this->template_data->set('cities', $cities->populate());
		
		$this->load->model(array("Places_category_model"));
		$categories = new $this->Places_category_model;
		$categories->setJoin('categories', 'categories.id=places_category.cat_id');
		$categories->setJoin('places', 'places.id=places_category.id');
		$categories->setOrder('places.lastmod', 'DESC');
		$categories->setSelect('categories.*');
		$categories->setGroupBy('categories.id');
		
		$categories_list = array();
		foreach( $categories->populate() as $cat ) {

			$cat_places = new $this->Places_category_model;
			$cat_places->setCatId( $cat->id, true );
			$cat_places->setLimit( 5 );
			$cat_places->setJoin( "places", "places.id = places_category.id" );
			$cat_places->setOrder('places.lastmod', 'DESC');
			$cat->places = $cat_places->populate();
			$categories_list[] = $cat;
		}
		$this->template_data->set('categories', $categories_list);

		$this->load->view('welcome_message', $this->template_data->get() );
	}
}
