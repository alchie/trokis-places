<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends MY_Controller {
	
	private $post_limit = 20;
	
    function __construct() 
    {
        parent::__construct();
                
		if( $this->session->userdata( 'id' ) != '10208772272039472') {
			redirect("home");
		}
		
    }
	
	private function _pagination($module='places', $total_rows=0, $per_page=20) {
		$this->pagination->initialize(array(
			'full_tag_open' => '<nav class="text-center"><ul class="pagination pagination-sm">', 'full_tag_close' => '</ul></nav>',
			'first_tag_open' => '<li>', 'first_tag_close' => '</li>',
			'prev_tag_open' => '<li>', 'prev_tag_close' => '</li>',
			'num_tag_open' => '<li>', 'num_tag_close' => '</li>',
			'cur_tag_open' => '<li class="active"><a href="#">', 'cur_tag_close' => '</a></li>',
			'next_tag_open' => '<li>', 'next_tag_close' => '</li>',
			'last_tag_open' => '<li>', 'last_tag_close' => '</li>',
			'base_url' => base_url('manage/'.$module.'/view/'), 
			'total_rows' => $total_rows,
			'per_page' => $per_page,
		)); 
		return $this->pagination->create_links();
	}
	
	public function places($action='view', $id=0) {
		$this->load->model(array("Places_model"));
		switch($action) {
			case 'edit':
				$place = new $this->Places_model;
				$place->setId($id, true, false);
				if( count($this->input->post()) > 0 ) {
					$this->form_validation->set_rules('name', 'Place Name', 'trim|required');					
					 if ($this->form_validation->run() == TRUE) {
						 $place->setName($this->input->post('name'));
						 $place->setLastmod(date('Y-m-d H:i:s'));
						 $place->setExclude(array('page_id','type','modby'));
						 $place->update();
					 }
				}
				$this->template_data->set('current_place', $place->get());
				$this->load->view('manage/places_edit', $this->template_data->get() );
			break;
			default:
			case 'view':
				$places = new $this->Places_model;
				$places->setLimit( $this->post_limit );
				$places->setStart($id);
				$places->setOrder('lastmod', 'DESC');
				
				if( $this->input->get('q') != '' ) {
					$places->setWhereOr("page_id LIKE '%" . $this->input->get('q') . "%'");
					$places->setWhereOr("name LIKE '%" . $this->input->get('q') . "%'");
				}
				
				if( $this->input->get('type') != '' ) {
					$places->setType($this->input->get('type'), true);
				}
				
				$this->template_data->set('all_places', $places->populate());
				$this->template_data->set('pagination', $this->_pagination('places', $places->count_all(), $this->post_limit));
				$this->load->view('manage/places', $this->template_data->get() );
			break;
		}
	}

	public function cities($action='view', $id=0) {
		$this->load->model(array("City_model"));
		switch($action) {
			case 'edit':
				$city = new $this->City_model;
				$city->setId($id, true, false);
				if( count($this->input->post()) > 0 ) {
					$this->form_validation->set_rules('name', 'City Name', 'trim|required');					
					 if ($this->form_validation->run() == TRUE) {
						 $city->setName($this->input->post('name'));
						 $city->update();
					 }
				}
				$this->template_data->set('current_city', $city->get());
				$this->load->view('manage/cities_edit', $this->template_data->get() );
			break;
			default:
			case 'view':
				$cities = new $this->City_model;
				$cities->setLimit( $this->post_limit );
				$cities->setStart($id);
				
				if( $this->input->get('q') != '' ) {
					$cities->setWhereOr("name LIKE '%" . $this->input->get('q') . "%'");
				}
				
				$this->template_data->set('all_cities', $cities->populate());
				$this->template_data->set('pagination', $this->_pagination('cities', $cities->count_all(), $this->post_limit));
				$this->load->view('manage/cities', $this->template_data->get() );
			break;
		}
	}

	public function categories($action='view', $id=0) {
		$this->load->model(array("Categories_model"));
		switch($action) {
			case 'edit':
				$category = new $this->Categories_model;
				$category->setId($id, true, false);
				if( count($this->input->post()) > 0 ) {
					$this->form_validation->set_rules('name', 'Category Name', 'trim|required');					
					 if ($this->form_validation->run() == TRUE) {
						 $category->setName($this->input->post('name'));
						 $category->setExclude('cat_id');
						 $category->update();
					 }
				}
				$this->template_data->set('current_category', $category->get());
				$this->load->view('manage/categories_edit', $this->template_data->get() );
			break;
			default:
			case 'view':
				$categories = new $this->Categories_model;
				$categories->setLimit( $this->post_limit );
				$categories->setStart($id);
				
				if( $this->input->get('q') != '' ) {
					$categories->setWhereOr("name LIKE '%" . $this->input->get('q') . "%'");
				}
				
				$this->template_data->set('all_categories', $categories->populate());
				$this->template_data->set('pagination', $this->_pagination('categories', $categories->count_all(), $this->post_limit));
				$this->load->view('manage/categories', $this->template_data->get() );
			break;
		}
	}
	
	public function users($action='view', $id=0) {
		$this->load->model(array("Accounts_model"));
		switch($action) {
			case 'edit':
				$user = new $this->Accounts_model;
				$user->setId($id, true);
				$this->template_data->set('current_user', $user->get());
				$this->load->view('manage/users_edit', $this->template_data->get() );
			break;
			default:
			case 'view':
				$users = new $this->Accounts_model;
				$users->setLimit( $this->post_limit );
				$users->setStart($id);
				
				if( $this->input->get('q') != '' ) {
					$users->setWhereOr("name LIKE '%" . $this->input->get('q') . "%'");
					$users->setWhereOr("email LIKE '%" . $this->input->get('q') . "%'");
				}
				
				$this->template_data->set('users', $users->populate());
				$this->template_data->set('pagination', $this->_pagination('users', $users->count_all(), $this->post_limit));
				$this->load->view('manage/users', $this->template_data->get() );
			break;
		}
	}
}
