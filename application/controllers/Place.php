<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Place extends MY_Controller {

	public function index($id, $slug)
	{
		
		$this->load->model("Places_model");
		$places_model = new $this->Places_model;
		$places_model->setOrder('places.id', 'DESC');
		$places_model->setId($id, true);
		$places_model->setJoin('places_description', 'places_description.id=places.id');
		$places_model->setJoin('places_location', 'places_location.id=places.id');
		$places_model->setJoin('places_picture', 'places_picture.id=places.id');
		$dPlace = $places_model->get();
		
		if( urlencode( url_title($dPlace->name) ) != $slug ) {
			redirect("home");
		}
		
		$this->load->model("City_model");
		$cities = new $this->City_model;
		$cities->setLimit(20);
		$this->template_data->set('cities', $cities->populate());
		
		$this->load->model("Places_category_model");
		$categories = new $this->Places_category_model;
		$categories->setId( $id, true );
		$categories->setJoin( 'categories', 'categories.id = places_category.cat_id' );
		$this->template_data->set('categories', $categories->populate());
								
		$this->template_data->set('place', $dPlace);
		$this->template_data->set('page_title', $dPlace->name);
		$this->load->view('place', $this->template_data->get() );
	}
	
	public function map($id, $slug)
	{
		
		$this->load->model("Places_model");
		$places_model = new $this->Places_model;
		$places_model->setOrder('places.id', 'DESC');
		$places_model->setId($id, true);
		$places_model->setJoin('places_description', 'places_description.id=places.id');
		$places_model->setJoin('places_location', 'places_location.id=places.id');
		$places_model->setJoin('places_picture', 'places_picture.id=places.id');
		$dPlace = $places_model->get();
		
		if( url_title($dPlace->name) != $slug ) {
			redirect("home");
		}
		
		$this->load->model("City_model");
		$cities = new $this->City_model;
		$this->template_data->set('cities', $cities->populate());
		
		$this->template_data->set('place', $dPlace);
		$this->template_data->set('page_title', $dPlace->name);
		$this->load->view('place_map', $this->template_data->get() );
	}
	
	public function manage($action='view') {
		
		switch($action) {
			
			default:
			case 'view':
			if( $this->session->userdata( 'id' ) != '10208772272039472') {
				redirect("home");
			}
			$this->load->view('manage/places', $this->template_data->get() );
			break;
		}
	}

	public function sitemap_index()
	{
		$limit = 1000;
		$this->load->model('Places_model');
		$this->Places_model->setWhere('name IS NOT NULL');
		$this->Places_model->setOrder('lastmod', 'DESC');
		$pages = ceil($this->Places_model->count_all_results() / $limit );
		$latest = $this->Places_model->get();
		$this->output->set_content_type('application/xml');
		
echo <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
XML;
for( $i=0; $i < $pages; $i++ ) {
	$url = base_url('sitemap-'.($i+1).'.xml');
	$lastmod = date('c', strtotime($latest->lastmod));
echo <<<XML
<sitemap>
<loc>{$url}</loc>
<lastmod>{$lastmod}</lastmod>
</sitemap>
XML;
}
echo <<<XML
</sitemapindex>
XML;
	}
	
	public function sitemap($page=1)
	{
		$start = ceil(1000/5) * ($page - 1);
		$this->load->model('Places_model');
		$this->Places_model->setWhere('name IS NOT NULL');
		$this->Places_model->setStart($start);
		$this->Places_model->setLimit(1000);
		$this->Places_model->setOrder('id', 'DESC');
		$places = $this->Places_model->populate();
		
		$this->output->set_content_type('application/xml');
echo <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
XML;
foreach($places as $place) {
	$url = site_url($place->id . "_" . url_title($place->name));
	$lastmod = date('c', strtotime($place->lastmod));
echo <<<XML
<url>
	<loc>{$url}</loc>
	<lastmod>{$lastmod}</lastmod>
	<changefreq>weekly</changefreq>
	<priority>1</priority>
</url>
XML;
}
echo <<<XML
</urlset>
XML;

	}
}
