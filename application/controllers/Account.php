<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	public function login()
	{
		if ( $this->facebook->is_authenticated() ) {
			$user = $this->facebook->request('get', '/me?fields=id,name,email');
			if (!isset($user['error']))
			{
				$user['logged_in'] = false;
				$this->load->model("Accounts_model");
				$new = new $this->Accounts_model;
				$new->setUserId( $user['id'], true);
				if( $new->nonEmpty() === FALSE ) {
					$this->session->set_userdata( $user );
					redirect("account/register");
					/* 
					if( ! isset( $user['email'] ) ) {
						$user['page_title'] = 'Enter Email Address';
						if( $this->input->post('email') ) {
							$this->load->library('form_validation');
							$this->form_validation->set_rules('email', 'Email', 'valid_email');
							if ($this->form_validation->run() == TRUE)
							{
								$new->setName($user['name']);
								$new->setEmail($this->input->post('email'));
								$new->insert();
								$user['account_id'] = $new->getId();
								$user['logged_in'] = true;
							} else {
								$this->load->view('register', $user);
							}
						} else {
							$this->load->view('register', $user);
						}
					} else {
						$new->setName($user['name']);
						$new->setEmail($user['email']);
						$new->insert();
						$user['account_id'] = $new->getId();
						$user['logged_in'] = true;
					}
					*/
				} else {
					$results = $new->getResults();
					$user['account_id'] = $results->id;
					$user['logged_in'] = true;
				}
				if( $user['logged_in'] == true ) {
					$this->session->set_userdata($user);
					redirect("home");
				}
			}			
		} else {
			redirect($this->facebook->login_url());
		}
	}

	public function register()
	{
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('name', 'Name', 'trim|required');					
			$this->form_validation->set_rules('email', 'Email', 'trim|required');			
			if ($this->form_validation->run() == TRUE) {
				$this->load->model("Accounts_model");
				$new = new $this->Accounts_model;
				$new->setUserId( $this->session->userdata('id') );
				$new->setName($this->input->post('name'));
				$new->setEmail($this->input->post('email'));
				$new->insert();
				
				$session['id'] = $this->session->userdata('id');
				$session['name'] = $this->input->post('name');
				$session['email'] = $this->input->post('email');
				$session['account_id'] = $new->getId();
				$session['logged_in'] = true;
				$this->session->set_userdata($session);
				redirect("home");
			}
		}
		
		if ( $this->facebook->is_authenticated() ) {
			$user = $this->facebook->request('get', '/me?fields=id,name,email');
			if (!isset($user['error']))
			{
				$user['page_title'] = 'Register';
				$user['logged_in'] = false;
				$this->load->view('register', $user);
			}
		} else {
			redirect( $this->facebook->login_url() );
		}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect("home");
		//redirect($this->facebook->logout_url());
	}
}
