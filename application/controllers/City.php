<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends MY_Controller {
	
	public function index($id, $slug)
	{
		$this->load->model(array("City_model", "Places_location_model"));
		$city = new $this->City_model;
		$city->setId( $id, true );
		$dCity = $city->get();
		
		if( url_title($dCity->name) != $slug ) {
			redirect('home');
		}
		$this->template_data->set('city', $dCity);
		
		$places = new $this->Places_location_model;
		$places->setCityId( $id, true );
		$places->setJoin( 'places', 'places.id=places_location.id' );
		$this->template_data->set('places', $places->populate());
		
		$cities = new $this->City_model;
		$cities->setLimit(20);
		$this->template_data->set('cities', $cities->populate());
	
		$this->load->view('city', $this->template_data->get() );
	}
	
	public function manage($action='view') {
		
		switch($action) {
			
			default:
			case 'view':
			if( $this->session->userdata( 'id' ) != '10208772272039472') {
				redirect("home");
			}
			$this->load->view('manage/cities', $this->template_data->get() );
			break;
		}
	}
}
