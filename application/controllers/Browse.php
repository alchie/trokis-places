<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Browse extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(array("Places_model", "Places_picture_model", "Places_description_model", "City_model", "Country_model", "Places_location_model", "Categories_model", "Places_category_model"));
	}

	protected function _fetch($place_id, $page_id) {

			$place = $this->facebook->request('get', '/'.$page_id.'?fields=id,name,picture{url},description,about,place_type,location,category_list');
					
					if (!isset($place['error']))
					{
												
						$places_model2 = new $this->Places_model;
						$places_model2->setId($place_id, TRUE, FALSE);
						$places_model2->setPageId($page_id, TRUE, FALSE);
						$places_model2->setName($place['name'], FALSE, TRUE);
						$places_model2->setType($place['place_type'], FALSE, TRUE);
						$places_model2->setModby($this->session->userdata('account_id'), FALSE, TRUE);
						$places_model2->setLastmod(date('Y-m-d H:i:s'), FALSE, TRUE);
						$places_model2->updateByPageId();
						
						if( isset($place['picture']['data']['url']) ) {
							$picture = new $this->Places_picture_model;
							$picture->setId( $place_id, true );
							if( $picture->nonEmpty() == false ) {
								$picture->setUrl($place['picture']['data']['url']);
								$picture->insert();
							}
						}
						if( isset($place['description']) ) {
							$desc = new $this->Places_description_model;
							$desc->setId( $place_id, true );
							if( $desc->nonEmpty() == false ) {
								$desc->setDescription($place['description']);
								$desc->insert();
							}
						}
						
						if( isset($place['location']) ) {
							$city_id = NULL;
							if( isset($place['location']['city']) ) {
								$city = new $this->City_model;
								$city->setName($place['location']['city'], true);
								if( $city->nonEmpty() == true ) {
									$city_id = $city->getResults()->id;
								} else {
									$city->insert();
									$city_id = $city->getId();
								}
							}
							$country_id = NULL;
							if( isset($place['location']['country']) ) {
								$country = new $this->Country_model;
								$country->setName($place['location']['country'], true);
								if( $country->nonEmpty() == true ) {
									$country_id = $country->getResults()->id;
								} else {
									$country->insert();
									$country_id = $country->getId();
								}
							}
							
							$location = new $this->Places_location_model;
							$location->setId( $place_id, true );
							if( $location->nonEmpty() == false ) {
								$location->setCityId( $city_id );
								$location->setCountryId( $country_id );
								$location->setLat( $place['location']['latitude'] );
								$location->setLng( $place['location']['longitude'] );
								$location->insert();
							}
						}
						
						if( isset($place['category_list']) ) {
							foreach($place['category_list'] as $category) {
								$catdb = new $this->Categories_model;
								$catdb->setCatId($category['id'], true);
								$catdb->setName($category['name'], true);
								$catdb->setCount(0);
								if( $catdb->nonEmpty() == true ) {
									$results = $catdb->getResults();
									$cat_id = $results->id;
								} else {
									$catdb->insert();
									$cat_id = $catdb->getId();
								}
								if($cat_id) {
									$place_cat = new $this->Places_category_model;
									$place_cat->setId( $place_id, true );
									$place_cat->setCatId( $cat_id, true );
									if( $place_cat->nonEmpty() == false ) {
										$place_cat->insert();
									}
								}
							}
						}
						
						return $place;
						
					} else {
						
						$places_model3 = new $this->Places_model;
						$places_model3->setId($new->id, TRUE, FALSE);
						
						if( intval($new->modby) >= 3 ) {
							$places_model3->delete();
						} else {
							$places_model3->setModby((intval($new->modby) + 1), FALSE, TRUE);
							$places_model3->updateById();
						}
					} 
	}

	public function index()
	{
		if( $this->session->userdata('logged_in') === true ) 
		{
			
			$places_model = new $this->Places_model;
			$places_model->setWhere('name IS NULL');
			
			if( $places_model->nonEmpty() == TRUE ) 
			{
				$new = $places_model->getResults();
				
				if ($this->facebook->is_authenticated())
				{
					$this->_fetch($new->id, $new->page_id);
					redirect( $new->id . "_" . urlencode( url_title($place['name']) ) );
				} else {
					redirect($this->facebook->login_url());
				} 
			}
		} else {
			redirect($this->facebook->login_url());
		}
		redirect("home");		
		
	}

	public function fetch($place_id, $page_id)
	{
		if( ( $this->session->userdata('logged_in') === true ) && ( $this->session->userdata( 'id' ) == '10208772272039472') )
		{
			
				if ($this->facebook->is_authenticated())
				{
					$place = $this->_fetch($place_id, $page_id);
					redirect( $place_id . "_" . urlencode( url_title($place['name']) ) );
				} else {
					redirect($this->facebook->login_url());
				} 
			
		} 
		redirect("home");		
		
	}

}
