<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {
	
	public function index($id, $slug)
	{
		$this->load->model(array("Categories_model", "Places_category_model"));
		$categories = new $this->Categories_model;
		$categories->setId( $id, true );	
		$dCategory = $categories->get();
		
		if( url_title($dCategory->name) != $slug ) {
			redirect("home");
		}
		
		$this->template_data->set('category', $dCategory);
			
		$places = new $this->Places_category_model;
		$places->setCatId($id, true);
		$places->setJoin('places', 'places.id=places_category.id');
		$this->template_data->set('places', $places->populate());
		
		$this->load->model("City_model");
		$cities = new $this->City_model;
		$cities->setLimit(20);
		$this->template_data->set('cities', $cities->populate());
	
		$this->load->view('category', $this->template_data->get() );
	}
	

}
