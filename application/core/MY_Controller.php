<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
        
    function __construct() 
    {
        parent::__construct();
                

        $this->template_data->set('page_title', 'Trokis Places - Trading Places in the Philippines!');
		
		$this->template_data->set('logged_in', ($this->session->userdata('logged_in')===TRUE));
		
    }
    
}