<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Template_data {
    
    var $CI;
    private $data = array();

    public function __construct()
    {
       $this->CI =& get_instance();
    }
            
    function set($key, $value)
    {
        $this->data[$key] = $value;                    
    }
    
    function get($key=false)
    {
        if($key) {
            return $this->data[$key];
        } else {
            return $this->data;
        }
    }
    
    function prepend($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $value . $current_value );  
    }
    
    function append($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $current_value . $value );        
    }
            
    function opengraph( $var=array() ) {
        $defaults = array(
            'title'=> $this->get('title'),
            'site_name'=>$this->CI->config->item('website_title'),
            'description'=>$this->CI->config->item('website_description'),
            'url'=> site_url( trim( uri_string(), "/") ),
            'type'=>'article',
            'image'=>'http://indavao.net/assets/images/logo.png',
            'image:width'=>'289',
            'image:height'=>'102',
        );
        
        $opengraph = array_merge( (array) $defaults, (array) $var );
        
        $meta = '<meta property="fb:app_id" content="' . $this->fb_app_id . '" />';
        foreach( $opengraph as $tag=>$value ) {
            if( $value != '' ) {
                $meta .= "\n" . '<meta property="og:'.$tag.'" content="'.$value.'" />';
                //$meta .= '<meta name="'.$tag.'" content="'.$value.'" />' . "\n";
                //$meta .= '<meta itemprop="'.$tag.'" content="'.$value.'" />' . "\n";                
            }
        }
        $this->set('opengraph', $meta );
    }
    
}

/* End of file Global_variables.php */