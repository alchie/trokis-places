<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class CI_Email_notification {
    
    private $config = array();
    private $subject = "InDavao.Net";
    private $recepient;
    private $sender = 'noreply@indavao.net';
    private $sender_name = 'InDavao.Net';
    private $text_message = '';
    private $html_message = '';
    
    function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->library('email');
        
        $this->CI->email->clear();
    }
    
    function setRecepient($email) {
        $this->recepient = $email;
        return $this;
    }
    
    function setTextMessage($message) {
        $this->text_message = $message;
        return $this;
    }
    
    function setHtmlMessage($message) {
        $this->html_message = $message;
        return $this;
    }

    function send() {
        $this->CI->load->helper('email');
        if( ( $this->recepient == '') 
            || ( $this->text_message == '') 
            || ( ! valid_email( $this->recepient ) )
        ){ 
            return;
        }
           
        $this->CI->email->initialize($this->config);
        $this->CI->email->from($this->sender, $this->sender_name);      
        $this->CI->email->to($this->recepient);
        $this->CI->email->subject($this->subject);
        $this->CI->email->message( $this->text_message );
        return $this->CI->email->send();

    }
    
    function welcome( $user ) {
        $this->config['priority'] = 1;
        $this->subject = "Welcome to InDavao.Net";
        $this->text_message = "Dear {$user->fullname},\n\nWelcome to InDavao.Net - The first online networking in Davao City!\n\nThank you for activating your account!\n\n\nRegards,\n\nChester Alan Tagudin\nFounder and CEO\nIndavao.Net";       
    }
    
    function activation($uid, $code) {
        $link = site_url( 'account/activation/' . $uid .'/' . $code );
        $this->config['priority'] = 1;
        $this->subject = "InDavao.Net Account Activation Link";
        $this->text_message = "Thanks for signing up for an InDavao.Net Account!\n\nActivation Code: {$code} \n\nClick the link below to activate your account.\n\n{$link}\n\nIf the link does not work, copy & paste it into your web browser's address bar.\n\nAre you ready to join the first online networking in Davao City? Activate your account now!\n\n\nKind Regards,\nThe InDavao.Net Team\n\nAlchie Netcafe, Christ the King St, Phase 2, Brgy. Sto. Nino, Tugbok, Davao City, Philippines, 8000";
      
    }
    
    function forgotPassword($uid, $code) {
        $link = site_url( 'account/resetpassword/' . $uid->user_id .'/' . $code );
        $this->config['priority'] = 1;
        $this->subject = "InDavao.Net Forgot Password Confirmation";
        $this->text_message = "Dear {$uid->fullname}, \n\nYou are receiving this message because you request to reset your password.\n\nClick the link below to reset your password.\n\n{$link}\n\nDisregard this message if you are not the one requesting it. \n\n\nKind Regards,\nThe InDavao.Net Team\n\nAlchie Netcafe, Christ the King St, Phase 2, Brgy. Sto. Nino, Tugbok, Davao City, Philippines, 8000";
      
    }
    
    function resetPassword($uid, $password) {
    
        $link = site_url( 'account/login');
        $this->config['priority'] = 1;
        $this->subject = "InDavao.Net New Password";
        $this->text_message = "Dear {$uid->fullname}, \n\nHere's your new password.\n\nNew Password: {$password}\n\nClick here to login: \n\n{$link}\n\n\nKind Regards,\nThe InDavao.Net Team\n\nAlchie Netcafe, Christ the King St, Phase 2, Brgy. Sto. Nino, Tugbok, Davao City, Philippines, 8000";
       
    }
    
}